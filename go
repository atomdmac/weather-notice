#!/bin/bash

APP_NAME="weather-notice"

function build {
  podman build . -t $APP_NAME:latest
  exit
}

function clean {
  podman container rm $APP_NAME
}

function nuke {
  # TODO: Double check with the user before nuking 😬
  clean
  podman image rm $APP_NAME
}

function nuke_db {
  stop
  rm -rf ./databases/*
  resume
}

function start {
  echo "Starting $APP_NAME and running in the background..."
  podman run \
    --detach \
    -it --name $APP_NAME\
    -v ./databases:/app/databases \
    -v ./docker:/app/docker \
    -v ./dist:/app/dist \
    -v ./config:/app/config \
    --tz $(cat /etc/timezone) \
    $APP_NAME:latest
  exit
}

function resume {
  echo "Resuming $APP_NAME from previously created container."
  podman start $APP_NAME
}

function stop {
  echo "Stopping $APP_NAME..."
  podman stop $APP_NAME
}

function shell {
  podman exec -it $APP_NAME /bin/bash
  exit
}

function logs {
  # Show the last 300 lines of the log and then follow any new logs that appear.
  podman logs --tail 300 -f $APP_NAME
}

function help {
  echo "Commands:"
  echo "- build"
  echo "- clean"
  echo "- nuke"
  echo "- nuke_db"
  echo "- start"
  echo "- resume"
  echo "- stop"
  echo "- shell"
  echo "- logs"
}

case "$1" in
  build) build ;;
  clean) clean ;;
  nuke) nuke ;;
  nuke_db) nuke_db ;;
  start) start ;;
  resume) resume ;;
  stop) stop ;;
  shell) shell ;;
  logs) logs ;;
  *) help ;;
esac
