import { WeatherEvent } from "./weather";
import { DateTime, Interval } from "luxon";
import { getDisplayDateTime, getSimpleDisplayTime } from "./utils";
import config from "./config";

export type AlertSpec = {
  id: string;
  codes: number[];
  hoursInAdvance: number;
};

export type AlertNotification = {
  title: string;
  message: string;
  alert: AlertSpec;
  event: WeatherEvent;
};

/**
 * Given an AlertSpec[] and a WeatherEvent[], determine which list of
 * WeatherEvent the user should be notified about.
 */
export function determineAlertNotifications(
  alerts: AlertSpec[],
  events: WeatherEvent[]
): AlertNotification[] {
  const now = DateTime.now();
  const getDeltaHours = (dateString: string) => {
    const delta = Interval.fromDateTimes(
      now,
      DateTime.fromISO(dateString, { zone: config.urlParameters.timezone })
    );
    return delta.length("hours");
  };

  return events.reduce(
    (notifications: AlertNotification[], event: WeatherEvent) => {
      return notifications.concat(
        alerts
          .filter((alert) => {
            const isWithinTimeInterval =
              getDeltaHours(event.time) <= alert.hoursInAdvance;
            const isRelevant = alert.codes.includes(event.code);
            return isWithinTimeInterval && isRelevant;
          })
          .map((alertSpec: AlertSpec) => ({
            title: event.description,
            message: `${Math.round(
              getDeltaHours(event.time)
            )} hours (${getSimpleDisplayTime(event.time)})`,
            alert: alertSpec,
            event,
          }))
      );
    },
    []
  );
}
