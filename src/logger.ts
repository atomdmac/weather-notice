import config from "./config";
import { getArguments } from "./utils";
import { DateTime } from "luxon";
import Rollbar, { LogArgument, Level } from "rollbar";

const rollbar = new Rollbar({
  accessToken: config.rollbar.accessToken,
  logLevel: config.rollbar.logLevel as Level,
  reportLevel: config.rollbar.logLevel as Level,
  captureUncaught: true,
  captureUnhandledRejections: true,
});

export function getTimestamp() {
  return DateTime.now().toString();
}

export function debug(...args: unknown[]) {
  rollbar.debug(...(args as LogArgument[]));
  if (getArguments().verbose) {
    console.log(getTimestamp(), ...args);
  }
}

export function info(...args: unknown[]) {
  rollbar.info(...(args as LogArgument[]));
  console.info(getTimestamp(), ...args);
}

export function error(...args: unknown[]) {
  rollbar.error(...(args as LogArgument[]));
  console.error(getTimestamp(), ...args);
}
