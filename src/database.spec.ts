import PouchDB from "pouchdb";
import PouchDBMemory from "pouchdb-adapter-memory";
import { AlertNotification } from "./alerts";
import { saveAlertReciepts } from "./database";

PouchDB.plugin(PouchDBMemory);

describe("database", function () {
  let db: PouchDB.Database;

  db = new PouchDB("test-db", { adapter: "memory" });
  beforeAll(async function () {});

  describe("saveAlertReciepts", function () {
    const alertNotifications: AlertNotification[] = [
      {
        title: "Weather Alert!",
        message: "Bad weather incoming!",
        event: {
          code: 1,
          description: "Bad weather",
          time: new Date().toISOString(),
        },
        alert: {
          id: "1",
          codes: [1],
          hoursInAdvance: 1,
        },
      },
    ];
    it("Should save alert records", async function () {
      await saveAlertReciepts(db, alertNotifications);
    });
    it("should update existing alert records", async function () {
      await saveAlertReciepts(db, alertNotifications);
    });
  });
});
