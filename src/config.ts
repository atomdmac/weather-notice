import { WeatherTypes } from "./weather";

export default {
  gotify: {
    accessToken: process.env.GOTIFY_ACCESS_TOKEN || "",
    url: process.env.GOTIFY_URL,
  },
  rollbar: {
    accessToken: process.env.ROLLBAR_ACCESS_TOKEN || "",
    logLevel: "info",
  },
  mailgun: {
    username: "api",
    key: process.env.MAILGUN_API_KEY || "",
    domain: process.env.MAILGUN_DOMAIN || "",
    to: process.env.MAILGUN_TO,
    from: process.env.MAILGUN_FROM,
    subject: "Weather Notice",
  },
  database: {
    name: "weather",
    path: "databases",
  },
  url: "https://api.open-meteo.com/v1/forecast",
  urlParameters: {
    latitude: process.env.LATITUDE || 0,
    longitude: process.env.LONGITUDE || 0,
    hourly: "weathercode",
    daily: "weathercode",
    temperature_unit: "fahrenheit",
    windspeed_unit: "mph",
    precipitation_unit: "inch",
    timezone: process.env.TIMEZONE || "America/New_York",
  },
};
