import PouchDB from "pouchdb";
import PouchDBFind from "pouchdb-find";
import { v4 as uuid } from "uuid";
import { DateTime, Duration } from "luxon";
import { AlertNotification } from "./alerts";
import { existsSync, mkdirSync } from "fs";
import * as logger from "./logger";

// Initialize the database.
PouchDB.plugin(PouchDBFind);

// Copied from PouchDB types since they don't seem to be exported for some reason?
interface PouchDbError {
  /**
   * HTTP Status Code during HTTP or HTTP-like operations
   */
  status?: number | undefined;
  name?: string | undefined;
  message?: string | undefined;
  reason?: string | undefined;
  error?: string | boolean | undefined;
  id?: string | undefined;
  rev?: string | undefined;
}

export type Doc = {
  id: string;
  docType: string;
  createdAt: string;
  modifiedAt: string;
};

export type AlertNotificationDoc = Doc &
  AlertNotification & {
    docType: "alertNotification";
  };

export function createDatabase({
  dbName,
  dbPath = "",
}: {
  dbName: string;
  dbPath: string;
}): PouchDB.Database {
  // create new directoryj
  try {
    // first check if directory already exists
    if (dbPath && !existsSync(dbPath)) {
      mkdirSync(dbPath);
      console.log("Directory is created.");
    } else {
      console.log("Directory already exists.");
    }
  } catch (err) {
    console.log(err);
  }
  return new PouchDB(`${dbPath}/${dbName}`);
}

export async function resetDatabase({
  db,
  dbName,
  dbPath,
}: {
  db: PouchDB.Database;
  dbName: string;
  dbPath: string;
}) {
  await db.destroy();
  return createDatabase({ dbName, dbPath });
}

export async function getAllAlertReciepts(db: PouchDB.Database) {
  return db.find({
    selector: {
      docType: "alertNotification",
    },
  });
}

export async function saveAlertReciepts(
  db: PouchDB.Database,
  alerts: AlertNotification[]
) {
  try {
    // Get previous saves so we can update existing records if necessary.
    const previousSaves = await Promise.all(
      alerts.map((alert) => {
        return db.get(getAlertId(alert)).catch(function () {
          return undefined;
        });
      })
    );

    // Create/update alert records in the database.
    const saves = alerts.map((alert, index) => {
      return db.put({
        _id: getAlertId(alert),
        docType: "alertNotification",
        _rev: previousSaves[index]?._rev,
        ...alert,
      });
    });
    return await Promise.all(saves);
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export enum PreviousAlerts {
  None = "None",
  Exact = "Exact",
  EventTypeChanged = "EventTypeChanged",
}

export async function hasAlerted(
  db: PouchDB.Database,
  alerts: AlertNotification[]
): Promise<PreviousAlerts[]> {
  let reciepts;
  // TODO: Get alert reciepts in batch transaction.
  try {
    reciepts = await Promise.all(
      alerts.map(async (alert) => {
        try {
          return (await db.get(getAlertId(alert))) as AlertNotificationDoc;
        } catch (error) {
          if ((error as PouchDbError).status === 404) {
            logger.debug("hasAlerted :: not found");
            return null;
          }
          throw error;
        }
      })
    );
  } catch (error) {
    logger.error("hasAlert :: Failed :: ", error);
    throw error;
  }

  return reciepts.map((reciept, index) => {
    if (reciept === null) {
      return PreviousAlerts.None;
    }

    if (reciept.event.code !== alerts[index].event.code) {
      return PreviousAlerts.EventTypeChanged;
    }

    return PreviousAlerts.Exact;
  });
}

export function getId(): string {
  return uuid();
}

export function getAlertId(alert: AlertNotification): string {
  const {
    event: { time },
  } = alert;
  return `${time}`;
}
