import Mailgun from "mailgun.js";
import FormData from "form-data";
import config from "./config";
import { AlertNotification } from "./alerts";
import { saveAlertReciepts } from "./database";
import * as logger from "./logger";
import { WeatherByCode } from "./weather";
import { DateTime } from "luxon";

const mg = new Mailgun(FormData).client({
  username: config.mailgun.username,
  key: config.mailgun.key,
});

export function getEmailMessage(alerts: AlertNotification[]) {
  const eventList = alerts.map(
    (alert) => `
          <tr>
          <td>${WeatherByCode[alert.event.code].description}</td>
          <td>${alert.message}</td>
          </tr>`
  );

  const systemTime = DateTime.fromJSDate(new Date(), {
    zone: config.urlParameters.timezone,
  });

  const message = `
    <h1>Incoming Weather Events</h1>
    <p>The following weather will be occurring in your area in the near future.</p>
    <table>
      <thead>
        <th>Weather</th>
        <th>Timeframe</th>
      </thead>
      <tbody>
        ${eventList.join("")}
      </tbody>
    </table>
    <p>The system time is currently ${systemTime}</p>
    <p>Good luck out there!</p>
    <p>- Weather Bot</p>
    <h2>Weather API Responses</h2>
    <code>
      ${JSON.stringify(alerts, null, 2)}
    </code>
`;
  const messageObj = {
    to: config.mailgun.to,
    from: config.mailgun.from,
    subject: config.mailgun.subject,
    text: message,
    html: message,
  };
  logger.debug("Created message: ", messageObj);

  return messageObj;
}

export async function sendAlerts(
  db: PouchDB.Database,
  alerts: AlertNotification[]
) {
  try {
    // Email Notifications
    const message = getEmailMessage(alerts);
    const response = await mg.messages.create(config.mailgun.domain, message);
    logger.debug("sendAlert :: MailGun :: ", response);
    await saveAlertReciepts(db, alerts);

    // Gotify Notifications
    alerts.forEach((alert) =>
      sendGotifyAlert({ title: alert.title, message: alert.message })
    );
  } catch (error) {
    logger.error("sendAlert :: ", error);
    throw error;
  }
}

export async function sendGotifyAlert({ title = "", message = "" }) {
  const params = new URLSearchParams();
  params.append("title", title);
  params.append("message", message);
  return await fetch(
    `${config.gotify.url}/message?token=${config.gotify.accessToken}`,
    {
      method: "POST",
      body: params,
    }
  ).catch(logger.error);
}
