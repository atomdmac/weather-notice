import fetch from "node-fetch";
import { DateTime } from "luxon";
import { WeatherEvent, WeatherByCode } from "./weather";
import * as logger from "./logger";
import config from "./config";

type ApiResponse = {
  longitude: number;
  latitude: number;
  elevation: number;
  utc_offset_seconds: number;
  daily_units: Units;
  hourly_units: Units;
  daily: Forecast;
  hourly: Forecast;
  generationtime_ms: number;
  error?: boolean;
  reason?: string;
};

type Forecast = {
  weathercode: number[];
  time: string[];
};

type Units = {
  weathercode: string;
  time: string;
};

export async function fetchData(url: string) {
  return await fetch(url);
}

export async function getWeatherData(
  url: string,
  fetchFn = fetchData
): Promise<WeatherEvent[]> {
  const response = await fetchFn(url);
  const json = (await response.json()) as ApiResponse;
  if (json.error) {
    logger.error(`getWeatherData :: API returned error: "${json.reason}"`);
    throw new Error(json.reason);
  }

  const { hourly } = json;

  logger.debug(
    `Weather API sent back ${hourly.weathercode.length} hourly records.`
  );
  const now = DateTime.now();
  const output: WeatherEvent[] = hourly.weathercode
    // Collate useful data
    .map((code: number, index: number) => ({
      code: code,
      time: hourly.time[index],
      description: WeatherByCode[code].description,
    }))
    // Only keep events that are relevant to our interests.
    .filter((weather: WeatherEvent) => {
      const occursAt = DateTime.fromISO(weather.time, {
        zone: config.urlParameters.timezone,
      });
      const isFuture = now < occursAt;
      return isFuture;
    });

  logger.debug(`${output.length} events are both relevant and in the future`);

  return output;
}
