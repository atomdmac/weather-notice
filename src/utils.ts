import { DateTime } from "luxon";

/**
 * Return a date string representation suitable for display to the user.
 */
export function getDisplayDateTime(dateTime: string): string {
  const parsedDate = DateTime.fromISO(dateTime);
  const formatted = parsedDate.toLocaleString(DateTime.DATETIME_MED);
  return formatted;
}

export function getSimpleDisplayTime(dateTime: string): string {
  const parsedDate = DateTime.fromISO(dateTime);
  const formatted = parsedDate.toLocaleString(DateTime.TIME_SIMPLE);
  return formatted;
}

/**
 * Given a flat object of key/value pairs, return a URL parameter string that
 * can be added to the end of a URL.
 */
export function objToUrl(obj: { [index: string]: string | number | boolean }) {
  const keys = Object.keys(obj);
  return keys.reduce((params: string, key: string) => {
    const safeKey = encodeURIComponent(key);
    const safeValue = encodeURIComponent(obj[key]);
    return `${params}&${safeKey}=${safeValue}`;
  }, "?");
}

/**
 * Parse CLI arguments and convert "--long" flags to key/value pairs
 */
export function getArguments(args: typeof process.argv = process.argv) {
  function toKeyValue(output: { [key: string]: unknown }, arg: string) {
    const pair = arg.split("=");
    output[pair[0].split("--")[1]] = pair[1] || true;
    return output;
  }
  return args.slice(2).reduce(toKeyValue, {});
}
