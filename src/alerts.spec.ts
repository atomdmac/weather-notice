import { AlertSpec, determineAlertNotifications } from "./alerts";
import { WeatherEvent } from "./weather";
import { DateTime } from "luxon";

describe("alerts", function () {
  describe("determineAlertNotifications", function () {
    it("should return only relevant events", function () {
      const alertSpec: AlertSpec = {
        id: "1",
        codes: [1],
        hoursInAdvance: 6,
      };
      const events: WeatherEvent[] = [
        {
          code: 1,
          description: "Relevant (Code + Future)",
          time: DateTime.now().plus({ hour: 1 }).toISO(),
        },
        {
          code: 1,
          description: "Irrelevant (To Far in Future)",
          time: DateTime.now().minus({ hour: 1 }).toISO(),
        },
        {
          code: 1,
          description: "Irrelevant (Past)",
          time: DateTime.now().minus({ hour: 1 }).toISO(),
        },
        {
          code: 2,
          description: "Irrelevant (Code)",
          time: DateTime.now().plus({ hour: 1 }).toISO(),
        },
      ];

      const result = determineAlertNotifications([alertSpec], events);

      expect(result.length).toEqual(1);
      expect(result[0]).toHaveProperty("alert");
      expect(result[0]).toHaveProperty("event");
      expect(result[0]).toHaveProperty("message");
    });
  });
});
