import notifier from "node-notifier";
import { WeatherTypes } from "./weather";
import { AlertSpec, AlertNotification } from "./alerts";
import { getArguments, objToUrl } from "./utils";
import { determineAlertNotifications } from "./alerts";
import config from "./config";
import { getWeatherData } from "./api";
import {
  createDatabase,
  hasAlerted,
  PreviousAlerts,
  resetDatabase,
} from "./database";
import { sendAlerts } from "./notifications";
import * as logger from "./logger";

// TODO: Move Alerts to config
const alerts: AlertSpec[] = [
  {
    id: "1",
    codes: [
      WeatherTypes.RAIN_SLIGHT.code,
      WeatherTypes.RAIN_MODERATE.code,
      WeatherTypes.RAIN_HEAVY.code,
      WeatherTypes.RAIN_SHOWERS_SLIGHT.code,
      WeatherTypes.RAIN_SHOWERS_MODERATE.code,
      WeatherTypes.RAIN_SHOWERS_VIOLENT.code,
      WeatherTypes.FREEZING_RAIN_LIGHT.code,
      WeatherTypes.FREEZING_RAIN_HEAVY.code,
      WeatherTypes.SNOW_SHOWERS_SLIGHT.code,
      WeatherTypes.SNOW_SHOWERS_MODERATE.code,
      WeatherTypes.SNOW_SLIGHT.code,
      WeatherTypes.SNOW_MODERATE.code,
      WeatherTypes.SNOW_HEAVY.code,
      WeatherTypes.THUNDERSTORM.code,
      WeatherTypes.THUNDERSTORM_HAIL_SLIGHT.code,
      WeatherTypes.THUNDERSTORM_HAIL_MODERATE.code,
    ],
    hoursInAdvance: 6,
  },
];

/**
 * Given a list of AlertNotifications, return only the ones that should be sent
 * to the user.  This exludes all events that the user was already notified of.
 */
async function getSendableAlerts(
  db: PouchDB.Database,
  alerts: AlertNotification[]
): Promise<AlertNotification[]> {
  logger.debug(
    `getSendableAlerts :: Checking ${alerts.length} alerts for sendability...`
  );
  let sendableAlerts: AlertNotification[] = [];
  if (alerts.length) {
    const existing = await hasAlerted(db, alerts);
    logger.debug(
      `getSendableAlerts :: Found ${existing.length} existing alerts.`
    );
    sendableAlerts = existing.reduce((sendable, status, index) => {
      if (
        status === PreviousAlerts.None ||
        status === PreviousAlerts.EventTypeChanged
      ) {
        sendable.push(alerts[index]);
      }
      return sendable;
    }, [] as AlertNotification[]);
  }
  logger.debug(
    `getSendableAlerts :: Found ${sendableAlerts.length} sendable alerts`
  );
  return sendableAlerts;
}

/**
 * The main program.
 */
async function main() {
  const cliArgs = getArguments(process.argv);
  logger.debug("Recieved arguments: ", cliArgs);

  let db = createDatabase({
    dbName: config.database.name,
    dbPath: config.database.path,
  });
  if (cliArgs.reset) {
    logger.debug("Resetting database...");
    db = await resetDatabase({
      db,
      dbName: config.database.name,
      dbPath: config.database.path,
    });
  }

  const url = `${config.url}${objToUrl(config.urlParameters)}`;
  logger.debug(`Fetching data from API at: ${url}`);
  const events = await getWeatherData(url);
  logger.info(`Got ${events.length} events from API`);

  const allAlerts = determineAlertNotifications(alerts, events);
  const sendableAlerts = await getSendableAlerts(db, allAlerts);
  if (sendableAlerts.length) {
    if (cliArgs["dry-run"]) {
      logger.debug("Dry run!");
      return;
    }
    await sendAlerts(db, sendableAlerts);
  } else {
    logger.info("No sendable alerts were found.");
  }
}

main();

notifier.notify({
  title: "Checking Weather",
  message: "Sit tight for your email!",
});
