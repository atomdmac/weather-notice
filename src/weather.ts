export const CLEAR_SKY = { code: 0, description: "Clear sky" };

// Cloud Cover
export const MAINLY_CLEAR = { code: 1, description: "Mainly clear" };
export const PARTLY_CLOUDY = { code: 2, description: "Partly cloudy" };
export const OVERCAST = { code: 3, description: "Overcast" };

// Fog
export const FOG = { code: 45, description: "Fog" };
export const DEPOSITING_RIME_FOG = {
  code: 48,
  description: "Depositing Rime Fog",
};

// Drizzle
export const DRIZZLE_SLIGHT = { code: 51, description: "Slight drizzle" };
export const DRIZZLE_MODERATE = { code: 53, description: "Moderate drizzle" };
export const DRIZZLE_HEAVY = { code: 55, description: "Heavy drizzle" };

export const DRIZZLE_FREEZING_LIGHT = {
  code: 56,
  description: "Freezing drizzle (Light)",
};
export const DRIZZLE_FREEZING_DENSE = {
  code: 57,
  description: "Freezing drizzle (Dense)",
};

// Rain
export const RAIN_SLIGHT = { code: 61, description: "Rain (Slight)" };
export const RAIN_MODERATE = { code: 63, description: "Rain (Moderate)" };
export const RAIN_HEAVY = { code: 65, description: "Rain (Heavy)" };

// Freezing Rain
export const FREEZING_RAIN_LIGHT = {
  code: 66,
  description: "Freezing rain (Light)",
};
export const FREEZING_RAIN_HEAVY = {
  code: 67,
  description: "Freezing rain (Heavy)",
};

// Snow
export const SNOW_SLIGHT = { code: 71, description: "Snow (Slight)" };
export const SNOW_MODERATE = { code: 73, description: "Snow (Moderate)" };
export const SNOW_HEAVY = { code: 75, description: "Snow (Heavy)" };

export const SNOW_GRAINS = { code: 77, description: "Snow grains" };

// Rain Showers
export const RAIN_SHOWERS_SLIGHT = {
  code: 80,
  description: "Rain showers (Slight)",
};
export const RAIN_SHOWERS_MODERATE = {
  code: 81,
  description: "Rain showers (Moderate)",
};
export const RAIN_SHOWERS_VIOLENT = {
  code: 82,
  description: "Rain showers (Violent)",
};

export const SNOW_SHOWERS_SLIGHT = {
  code: 85,
  description: "Snow showers (Slight)",
};
export const SNOW_SHOWERS_MODERATE = {
  code: 86,
  description: "Snow showers (Moderate)",
};

// Thunderstorms
export const THUNDERSTORM = { code: 95, description: "Thunderstorm (Slight)" };

export const THUNDERSTORM_HAIL_SLIGHT = {
  code: 96,
  description: "Thunderstorm with Hail (Slight)",
};
export const THUNDERSTORM_HAIL_MODERATE = {
  code: 99,
  description: "Thunderstorm with Hail (Moderate)",
};

export type WeatherType = {
  code: number;
  description: string;
};

export type WeatherEvent = WeatherType & {
  time: string;
};

export const WeatherTypes = {
  CLEAR_SKY,
  MAINLY_CLEAR,
  PARTLY_CLOUDY,
  OVERCAST,
  FOG,
  DEPOSITING_RIME_FOG,
  DRIZZLE_SLIGHT,
  DRIZZLE_MODERATE,
  DRIZZLE_HEAVY,
  DRIZZLE_FREEZING_LIGHT,
  DRIZZLE_FREEZING_DENSE,
  RAIN_SLIGHT,
  RAIN_MODERATE,
  RAIN_HEAVY,
  FREEZING_RAIN_LIGHT,
  FREEZING_RAIN_HEAVY,
  SNOW_SLIGHT,
  SNOW_MODERATE,
  SNOW_HEAVY,
  SNOW_GRAINS,
  RAIN_SHOWERS_SLIGHT,
  RAIN_SHOWERS_MODERATE,
  RAIN_SHOWERS_VIOLENT,
  SNOW_SHOWERS_SLIGHT,
  SNOW_SHOWERS_MODERATE,
  THUNDERSTORM,
  THUNDERSTORM_HAIL_SLIGHT,
  THUNDERSTORM_HAIL_MODERATE,
};

export const WeatherTypesList: WeatherType[] = [
  CLEAR_SKY,
  MAINLY_CLEAR,
  PARTLY_CLOUDY,
  OVERCAST,
  FOG,
  DEPOSITING_RIME_FOG,
  DRIZZLE_SLIGHT,
  DRIZZLE_MODERATE,
  DRIZZLE_HEAVY,
  DRIZZLE_FREEZING_LIGHT,
  DRIZZLE_FREEZING_DENSE,
  RAIN_SLIGHT,
  RAIN_MODERATE,
  RAIN_HEAVY,
  FREEZING_RAIN_LIGHT,
  FREEZING_RAIN_HEAVY,
  SNOW_SLIGHT,
  SNOW_MODERATE,
  SNOW_HEAVY,
  SNOW_GRAINS,
  RAIN_SHOWERS_SLIGHT,
  RAIN_SHOWERS_MODERATE,
  RAIN_SHOWERS_VIOLENT,
  SNOW_SHOWERS_SLIGHT,
  SNOW_SHOWERS_MODERATE,
  THUNDERSTORM,
  THUNDERSTORM_HAIL_SLIGHT,
  THUNDERSTORM_HAIL_MODERATE,
];

export const WeatherByCode = WeatherTypesList.reduce(
  (all: { [index: number]: WeatherType }, code: WeatherType) => {
    all[code.code] = code;
    return all;
  },
  {}
);
