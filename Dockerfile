FROM docker.io/node:latest
RUN apt update && apt install cron logrotate -y

# Dev-time software.  Remove for prod!
RUN apt install vim less -y

# Prep working directory area
# NOTE: log file placeholders will be created in the entrypoint.
RUN mkdir /app
RUN mkdir /app/logs
WORKDIR /app

# Bring in application files
COPY ./package.json .
COPY ./yarn.lock .
COPY ./tsconfig.json .
COPY ./src/ ./src/
COPY ./docker/ ./docker
COPY ./config ./config

# Do initial build.
RUN yarn && yarn build

# Install cron job
RUN ln -s /app/docker/cron/check-weather /etc/cron.d/check-weather
RUN ln -s /app/docker/cron/update-app /etc/cron.d/update-app
RUN ln -s /app/docker/cron/logrotate /etc/cron.d/logrotate

# cron jobs in /etc/cron.d *must* not be writable by group/other
RUN chmod go-wx /etc/cron.d/check-weather
RUN chmod go-wx /etc/cron.d/update-app
RUN chmod go-wx /etc/cron.d/logrotate

ENTRYPOINT ["/bin/sh", "/app/docker/entrypoint.sh"]
