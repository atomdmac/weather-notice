# Ensure that cron is started
service cron start

# Ensure that log files exist
touch /app/docker/logs/check-weather.log
touch /app/docker/logs/update-app.log
touch /app/docker/logs/logrotate.logs

# Watch logs!
tail -f --retry /app/docker/logs/*
