#!/usr/bin/env sh

PATH="/usr/sbin:$PATH"
SCRIPT_PATH="/app"

# Explicity change the current working directory to the project since node
# uses relative paths to resolve modules, etc.
cd $SCRIPT_PATH

/usr/sbin/logrotate /app/docker/cron/logrotate.conf
