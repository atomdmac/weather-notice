#!/usr/bin/env sh
set -euo

PATH="/usr/local/bin:$PATH"
APP_PATH="/app"

# Since this script runs via Cron, environment variables will need to be 
# imported manually from the .env file.
# VARS=$(cat "$APP_PATH/config/.env")
# for V in $VARS;
# do
  # export $V
# done

# Explicity change the current working directory to the project since node
# uses relative paths to resolve modules, etc.
cd $APP_PATH

# Run the pre-compiled app.
yarn run-prod --verbose
