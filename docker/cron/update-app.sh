#!/usr/bin/env sh

PATH="/usr/bin:/usr/local/bin:$PATH"
SCRIPT_PATH="/app"

# Explicity change the current working directory to the project since node
# uses relative paths to resolve modules, etc.
cd $SCRIPT_PATH

# Update from repo
git stash save "Backup at $(date)"
git pull

# Re-build!
yarn install
